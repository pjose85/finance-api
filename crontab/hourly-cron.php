<?php

use yii2tech\crontab\CronJob;
use yii2tech\crontab\CronTab;

$cronJob = new CronJob();
//$cronJob->min = '1';
//$cronJob->hour = '0';
//$cronJob->command = 'php getCurrencyAction.php';
//$cronJob->setLine('0 0 * * * php /path/to/my/project/yii some-cron');

$cronTab = new CronTab();
$cronTab->setJobs([
    $cronJob
]);
//$cronTab->apply();
