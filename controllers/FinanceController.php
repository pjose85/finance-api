<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use Scheb\YahooFinanceApi\ApiClient;
use Scheb\YahooFinanceApi\ApiClientFactory;
use GuzzleHttp\Client;

class FinanceController extends Controller
{
	/**
	 * getCurrency 
	 */
	public function actionCurrency()
	{
		try {
			$client = ApiClientFactory::createApiClient();
			$exchangeRates = $client->getExchangeRates([
				["USD", "EUR"],
				["USD", "JPY"],
				["USD", "CAD"],
				["USD", "GBP"],
				["USD", "MXN"],
			]);
		} catch (Exception $e) {
			var_dump("Error on getCurrency function");
		}
		$euroExChange = 0;
		$japaneseExChange = 0;
		$canadianExChange = 0;
		$britishExChange = 0;
		$mexicanExChange = 0;
		foreach ($exchangeRates as $exchange) {
			switch ($exchange->getCurrency()) {
			case "EUR":
				$euroExChange = $exchange->getAsk();
				break;
			case "JPY":
				$japaneseExChange = $exchange->getAsk();
				break;
			case "CAD":
				$canadianExChange = $exchange->getAsk();
				break;
			case "GBP":
				$britishExChange = $exchange->getAsk();
				break;
			case "MXN":
				$mexicanExChange = $exchange->getAsk();
				break;
			}
		}
		$argumments = [
			"euroExChange" => $euroExChange,
			"japaneseExChange" => $japaneseExChange,
			"canadianExChange" => $canadianExChange,
			"britishExChange" => $britishExChange,
			"mexicanExChange" => $mexicanExChange,
		];
		$this->writeFile($argumments);
	}

	/**
	 * writeFile Write a file in specifict location with the provided content
	 * @param array $dataContent values to save in the file
	 * @param string $path place to save the value default currencyInfo folder
	 */
	private function writeFile( array $dataContent, $path =  "@webroot/assets/currencyInfo/currency")
	{
		try {
			$jsonData = json_encode($dataContent);
			$jsonfile = Yii::getAlias($path) . date("Y-m-d-H") . '.json';
			$fp = fopen($jsonfile, 'w+');
			fwrite($fp, $jsonData);
			fclose($fp);
		} catch (Exception $e) {
			var_dump("Error on writeFile function");
		}
	}
}