<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

$this->title = 'Chart';
$this->params['breadcrumbs'][] = $this->title;

$dayArray = [];
$euroAskArray = [];
$janAskArray = [];
$caDollarAskArray = [];
$britishAskArray = [];
$mxPesoAskArray = [];

$searchDirectory = "@webroot/assets/currencyInfo/";
$arrayDirectory = array_diff(scandir(Yii::getAlias($searchDirectory)), array('..', '.'));

foreach ($arrayDirectory as $directory) {
	$jsonfile = Yii::getAlias('@webroot/assets/currencyInfo/' . $directory);
	if (file_exists($jsonfile)) {
		$fp = fopen($jsonfile, 'r');
		$content = fread($fp, filesize($jsonfile));
		$objectContent = json_decode($content);
		fclose($fp);
		if (!empty($objectContent)) {
			$argumments = [
				"euroExChange" => $objectContent->euroExChange,
				"japaneseExChange" => $objectContent->japaneseExChange,
				"canadianExChange" => $objectContent->canadianExChange,
				"britishExChange" => $objectContent->britishExChange,
				"mexicanExChange" => $objectContent->mexicanExChange,
			];
			$day = str_replace(["currency", ".json"],"", $directory);
			$dayArray[] = $day;
			$euroAskArray[] = $objectContent->euroExChange;
			$janAskArray[] = $objectContent->japaneseExChange;
			$caDollarAskArray[] = $objectContent->canadianExChange;
			$britishAskArray[] = $objectContent->britishExChange;
			$mxPesoAskArray[] = $objectContent->mexicanExChange;
		}
	}
}
if (!empty($dayArray)) {
	echo Highcharts::widget([
	   'options' => [
			'title' => ['text' => 'Dollar USA vs Euro'],
			'xAxis' => [
				'categories' => $dayArray,
			],
			'yAxis' => [
				'title' => ['text' => 'Dollar exchange'],
			],
			'series' => [
				['name' => 'Dollar Value', 'data' => $euroAskArray],
			],
		],
	]);
	echo Highcharts::widget([
	   'options' => [
			'title' => ['text' => 'Dollar USA vs Yan'],
			'xAxis' => [
				'categories' => $dayArray,
			],
			'yAxis' => [
				'title' => ['text' => 'Dollar exchange'],
			],
			'series' => [
				['name' => 'Dollar Value', 'data' => $janAskArray],
			],
		],
	]);
	echo Highcharts::widget([
	   'options' => [
			'title' => ['text' => 'Dollar USA vs Dollar CA'],
			'xAxis' => [
				'categories' => $dayArray,
			],
			'yAxis' => [
				'title' => ['text' => 'Dollar exchange'],
			],
			'series' => [
				['name' => 'Dollar Value', 'data' => $caDollarAskArray],
			],
		],
	]);
	echo Highcharts::widget([
	   'options' => [
			'title' => ['text' => 'Dollar USA vs British Pound'],
			'xAxis' => [
				'categories' => $dayArray,
			],
			'yAxis' => [
				'title' => ['text' => 'Dollar exchange'],
			],
			'series' => [
				['name' => 'Dollar Value', 'data' => $britishAskArray],
			],
		],
	]);
	echo Highcharts::widget([
	   'options' => [
			'title' => ['text' => 'Dollar USA vs Mexican Peso'],
			'xAxis' => [
				'categories' => $dayArray,
			],
			'yAxis' => [
				'title' => ['text' => 'Dollar exchange'],
			],
			'series' => [
				['name' => 'Dollar Value', 'data' => $mxPesoAskArray],
			],
		],
	]);
}
?>

