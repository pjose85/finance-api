<?php
define('__ROOT__',realpath(dirname(__FILE__) . '/../../'));

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Finance';
$loader = new FilesystemLoader(__ROOT__ . '/templates');
$twig = new Environment($loader);

Yii::$app->runAction('finance/currency');

$jsonfile = Yii::getAlias('@webroot/assets/currencyInfo/currency' . date("Y-m-d-H") . '.json');
if (file_exists($jsonfile)) {
    $fp = fopen($jsonfile, 'r');
    $content = fread($fp, filesize($jsonfile));
    $objectContent = json_decode($content);
    fclose($fp);
    if (!empty($objectContent)) {
        $argumments = [
            "euroExChange" => $objectContent->euroExChange,
            "japaneseExChange" => $objectContent->japaneseExChange,
            "canadianExChange" => $objectContent->canadianExChange,
            "britishExChange" => $objectContent->britishExChange,
            "mexicanExChange" => $objectContent->mexicanExChange,
        ];
        echo $twig->render('home.twig', $argumments);
        echo Html::a('Render Chart', ['/chart/currency'], ['class'=>'btn btn-primary']);
    }
}
?>